﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GameController : NetworkBehaviour
{

    [SerializeField] GameObject[] players;

    public void ResetPositions()
    {
        RpcResetPositions();
    }

    [ClientRpc]
    private void RpcResetPositions()
    {
        players = GameObject.FindGameObjectsWithTag("Player");
        var ball = GameObject.FindGameObjectWithTag("Ball");
        foreach (var item in players)
        {
            item.GetComponent<PlayerMovement>().ResetPosition();
        }
        ball.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        ball.transform.position = Vector2.zero;
    }
}
