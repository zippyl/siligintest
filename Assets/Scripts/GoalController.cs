﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GoalController : MonoBehaviour
{
    public ScoreScript.Score goalScore;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Ball")
        {
            FindObjectOfType<ScoreScript>().Increment(goalScore);
            FindObjectOfType<GameController>().ResetPositions();
        }
    }
}
