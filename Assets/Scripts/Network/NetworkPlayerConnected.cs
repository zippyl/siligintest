﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class NetworkPlayerConnected : NetworkBehaviour
{

    [SyncVar] public int playerConnected = 0;

    [SerializeField] private Transform[] boundaries;

    public void AddPlayer()
    {
        playerConnected++;
    }

    public void DecPlayer()
    {
        playerConnected--;
    }

    public int GetConnectedPlayers()
    {
        return NetworkServer.connections.Count;
    }

    public Transform GetBoundary()
    {
        return boundaries[playerConnected];
    }
}
