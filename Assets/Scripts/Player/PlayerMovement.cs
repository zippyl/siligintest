﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerMovement : NetworkBehaviour
{

    private bool wasJustClicked = true;
    private bool canMove;
    private Text playerScore;
    [SerializeField] private Vector2 startPosition;

    private Rigidbody2D rb;
    private Transform BoundaryHolder;
    private Boundary playerBoundary;
    private NetworkPlayerConnected networkPlayerConnected;

    struct Boundary
    {
        public float Up, Down, Left, Right;

        public Boundary(float up, float down, float left, float right)
        {
            Up = up; Down = down; Left = left; Right = right;
        }
    }

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        networkPlayerConnected = FindObjectOfType<NetworkPlayerConnected>();

        if (isLocalPlayer)
        {
            Debug.Log(networkPlayerConnected.playerConnected);
            startPosition = transform.position;
            BoundaryHolder = networkPlayerConnected.GetBoundary();

            playerBoundary = new Boundary(BoundaryHolder.GetChild(0).position.y,
                                      BoundaryHolder.GetChild(1).position.y,
                                      BoundaryHolder.GetChild(2).position.x,
                                      BoundaryHolder.GetChild(3).position.x);
        }
        networkPlayerConnected.AddPlayer();
    }

    // Update is called once per frame
    void Update()
    {
        if (!isLocalPlayer)
        {
            return;
        }
        if (Input.GetMouseButton(0))
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            if (wasJustClicked)
            {
                wasJustClicked = false;

                if (rb.OverlapPoint(mousePos))
                {
                    canMove = true;
                }
                else
                {
                    canMove = false;
                }
            }

            if (canMove)
            {
                Vector2 clampedMousePos = new Vector2(Mathf.Clamp(mousePos.x, playerBoundary.Left,
                                                                  playerBoundary.Right),
                                                      Mathf.Clamp(mousePos.y, playerBoundary.Down,
                                                                  playerBoundary.Up));
                rb.MovePosition(clampedMousePos);
            }
        }
        else
        {
            wasJustClicked = true;
        }
    }

    public override void OnStartLocalPlayer()
    {
        GetComponent<SpriteRenderer>().material.color = Color.yellow;
    }

    public void ResetPosition()
    {
        canMove = false;
        rb.position = startPosition;
    }

    private void OnDisable()
    {
        networkPlayerConnected.DecPlayer();
    }
}
