﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ScoreScript : NetworkBehaviour
{

    private void Start()
    {
        Time.timeScale = 1f;
    }

    public enum Score
    {
        Player1SCore, Player2Score
    }

    [SerializeField] private GameObject gameOverPanel;
    [SerializeField] private Text player1ScoreText, player2ScoreText;
    [SyncVar] public int player1Score, player2Score;

    public void GameOver()
    {
        CmdGameOver();
    }

    [Command]
    private void CmdGameOver()
    {
        RpcCheckIfGameOver();
    }

    public void Increment(Score score)
    {
        RpcIncrement(score);
    }

    [ClientRpc]
    private void RpcIncrement(Score score)
    {
        if (score == Score.Player1SCore)
            player1ScoreText.text = (++player1Score).ToString();
        else
            player2ScoreText.text = (++player2Score).ToString();
        CmdGameOver();
    }

    [ClientRpc]
    private void RpcCheckIfGameOver()
    {
        if (player1Score >= 5 || player2Score >= 5)
        {
            Time.timeScale = 0.00000000001f;
            gameOverPanel.SetActive(true);
        }
    }
}
