﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class UIController : NetworkBehaviour
{

    [SerializeField] private GameObject pausePanel;

    public void PauseState(bool state)
    {
        CmdPause(state);
    }

    [Command]
    public void CmdPause(bool state)
    {
        RpcPause(state);
    }

    [ClientRpc]
    public void RpcPause(bool state)
    {
        pausePanel.SetActive(state);
        if (state)
        {
            Time.timeScale = 0.0000000001f;
        }
        else
        {
            Time.timeScale = 1f;
        }
    }

    public void Disconnect()
    {
        NetworkManager.singleton.client.Disconnect();
    }
}
